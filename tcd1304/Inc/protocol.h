
/*Wersja dla blue pill USB CDC:*/
#ifndef __protocol_H
#define __protocol_H

#include "main.h"
#include "usbd_def.h"
#define USART_SendData(USART2, C) CDC_Transmit_FS(C,1);

/*******************/


#define PROTOCOL_DRE_ISR_bm  USART_DREINTLVL_LO_gc
#define sync_str "data:"
#define sync_len (sizeof(sync_str) - 1)

#define PROTOCOL_state_size0 (sync_len)
#define PROTOCOL_state_size1 (sync_len + 1)
#define PROTOCOL_state_data0 (sync_len + 2)
#define PROTOCOL_state_data_rest (sync_len + 3)

#define PROTOCOL_send_str(S) PROTOCOL_send_async((const unsigned char*)S,sizeof(S))


uint8_t PROTOCOL_is_sync(uint8_t data);

uint8_t _crc_ibutton_update(uint8_t crc, uint8_t data);

uint8_t Crc8CQuick(uint8_t Crc, int Size, uint8_t *Buffer);

uint8_t PROTOCOL_send_async (const unsigned char *data, uint16_t size);


/* receives packet and returns its size or zero on error */
/* buf_len must be greater or equal to size of packet */
int16_t PROTOCOL_get (unsigned char *buf, uint16_t buf_len, uint8_t read_byte);
#endif /*__ protocol_H */
