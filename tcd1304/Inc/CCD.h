/*
 * CCD.h
 *
 *  Created on: 10.03.2018
 *      Author: Mateusz
 */



#ifndef CCD_H_
#define CCD_H_

/* Includes */
#include "stm32f1xx.h"
#include "stm32f1xx_hal.h"
#include "protocol.h"

/* Settings */
#define fCLK				72	//TIM clock in MHz
#define fM					2000000
#define CCD_size			3694
#define SH_delay			0	//delays are for synchronization purposes; (ns)
#define ICG_delay			500
#define fM_delay			0
#define ADC_delay			0
#define ICG_period0			15999	//should be multiple of the SH_period
#define SH_period0			15999
#define ICG_prescaler0		35	//72 MHz timer clock -> 2MHz
#define SH_prescaler0		35
#define ICG_pulse			10	//(us)
#define SH_pulse			5	//(us)

#define CCD_Settings_id			'S' //83
#define CCD_Settings_size		sizeof(CCD_SettingsTypeDef)

#define CCD_Struct_id				'D'
#define CCD_Struct_data_size		(2*CCD_size - sync_len - 4)
#define CCD_Struct_packet_size		(2*CCD_size - sync_len - 3)
#define CCD_Struct_size				sizeof(CCD_StructTypeDef)
#define crc0						0xFF

/* CCD Settings structure */
typedef struct __attribute__((__packed__ ))
{
	char id;
	uint16_t SH_period;
	uint16_t ICG_period;
	uint16_t SH_prescaler;
	uint16_t ICG_prescaler;
	uint16_t N_avg;
} CCD_SettingsTypeDef;

/* PROTOCOL compatible CCD data structure */
typedef struct __attribute__((__packed__ ))
{
	char sync_s[sync_len];
	uint16_t packet_size;
	char id;
	uint8_t data[CCD_Struct_data_size];
	uint8_t crc8;
} CCD_StructTypeDef;

/* same buffer for ADC DMA and Tx (compatible with PROTOCOL) */
typedef union __attribute__((__packed__ ))
{
	uint16_t data[2*CCD_size];
	struct __attribute__((__packed__ ))
    {
       CCD_StructTypeDef low;
       CCD_StructTypeDef high;
    } data_struct;
} CCD_DataTypeDef;

/* CCD_Data section selector */
typedef enum
{
    low = 0,
    high = 1
} Section;

/* Prepare CCD_Struct for Tx */
void CCD_Struct_Prepare(CCD_StructTypeDef* CCD_Struct);

/* Prepare CCD_Data for Tx */
void CCD_Data_Prepare(CCD_DataTypeDef* CCD_Data, Section s);


extern CCD_SettingsTypeDef CCD_settings;
extern CCD_DataTypeDef CCD_data;

#endif /* CCD_H_ */
