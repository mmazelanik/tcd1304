/*
 * protocol.c
 *
 *  Created on: 11.03.2018
 *      Author: Mateusz
 */

#include "protocol.h"

volatile uint8_t PROTOCOL_state = 0;
static volatile uint16_t PROTOCOL_sender_state = 0;

volatile uint16_t PROTOCOL_packet_size = 0;
volatile uint16_t PROTOCOL_current_index = 0;
volatile uint8_t PROTOCOL_packet_crc8 = 0;

volatile uint16_t PROTOCOL_sender_packet_size = 0;
volatile uint16_t PROTOCOL_sender_data_size = 0;
volatile uint8_t PROTOCOL_sender_packet_crc8 = 0;
static const unsigned char *PROTOCOL_sender_buf = 0;

uint8_t PROTOCOL_is_sync(uint8_t data)
{
    if (sync_len <= PROTOCOL_state) {
        return 1;
    }
    if (sync_str[PROTOCOL_state] == data) {
        PROTOCOL_state++;
    } else {
        PROTOCOL_state = 0;
    }
    return 0;
}

uint8_t _crc_ibutton_update(uint8_t crc, uint8_t data)
{
    uint8_t i;

    crc = crc ^ data;
    for (i = 0; i < 8; i++) {
        if (crc & 0x01)
            crc = (crc >> 1) ^ 0x8C;
        else
            crc >>= 1;
    }

    return crc;
}

uint8_t Crc8CQuick(uint8_t Crc, int Size, uint8_t *Buffer)
{
  static const uint8_t CrcTable[] = { // Nibble table for polynomial 0x8C
    0x00,0x9D,0x23,0xBE,0x46,0xDB,0x65,0xF8, // sourcer32@gmail.com
    0x8C,0x11,0xAF,0x32,0xCA,0x57,0xE9,0x74 };

  while(Size--)
  {
    Crc ^= *Buffer++; // Apply Data
    Crc = (Crc >> 4) ^ CrcTable[Crc & 0x0F]; // Two rounds of 4-bits
    Crc = (Crc >> 4) ^ CrcTable[Crc & 0x0F];
  }

  return(Crc);
}

uint8_t PROTOCOL_send_async (const unsigned char *data, uint16_t size)
{
    //PROTOCOL_sender_state = 1;
    PROTOCOL_sender_state = 0;
    PROTOCOL_sender_buf = data;
    PROTOCOL_sender_packet_size = size;
    PROTOCOL_sender_packet_crc8 = 0xFF;

    uint8_t i;
    for (i = 0; i < size + 10; i++) {
        //while (!(USART2 ->SR & 0x00000040)); //oczekiwanie na gotowosc - ignorujemy w wersji dla nucleo

       // USART_SendData(USART2, sync_str[0]);

        if (sync_len > PROTOCOL_sender_state) {

            USART_SendData(USART2, sync_str[PROTOCOL_sender_state]);

        } else if (PROTOCOL_state_size0 == PROTOCOL_sender_state) {

            USART_SendData(USART2, PROTOCOL_sender_packet_size >> 8);

        } else if (PROTOCOL_state_size1 == PROTOCOL_sender_state) {

            USART_SendData(USART2, 0xFF & PROTOCOL_sender_packet_size);

        } else if (PROTOCOL_state_data0 + PROTOCOL_sender_packet_size > PROTOCOL_sender_state) {

            PROTOCOL_sender_packet_crc8 = _crc_ibutton_update(PROTOCOL_sender_packet_crc8, *PROTOCOL_sender_buf);
            USART_SendData(USART2, *PROTOCOL_sender_buf++);

        } else if (PROTOCOL_state_data0 + PROTOCOL_sender_packet_size == PROTOCOL_sender_state) {

            USART_SendData(USART2, PROTOCOL_sender_packet_crc8);

        } else {

            PROTOCOL_sender_state = 0;
            return 0;
        }
        PROTOCOL_sender_state++;
    }
    return 0;
}


/* receives packet and returns its size or zero on error */
/* buf_len must be greater or equal to size of packet */
int16_t PROTOCOL_get (unsigned char *buf, uint16_t buf_len, uint8_t read_byte)
{
    if (!PROTOCOL_is_sync(read_byte))
    {
        return 0;
    }
    if (PROTOCOL_state_size0 == PROTOCOL_state)
    {
        PROTOCOL_packet_size = read_byte << 8;
        PROTOCOL_state++;
    }
    else if (PROTOCOL_state_size1 == PROTOCOL_state)
    {
        PROTOCOL_packet_size |= read_byte;
        PROTOCOL_state++;
    }
    else if (PROTOCOL_state_data0 == PROTOCOL_state)
    {
        if (!PROTOCOL_packet_size || PROTOCOL_packet_size > buf_len)
        {
            // empty packet or too small buffer
            PROTOCOL_state = 0;
            return 0;
        }
        PROTOCOL_state++;
        PROTOCOL_packet_crc8 = _crc_ibutton_update(0xFF, read_byte);
        buf[0] = read_byte;
        PROTOCOL_current_index = 1;
    }
    else if (PROTOCOL_state_data_rest == PROTOCOL_state)
    {
        if (PROTOCOL_current_index < PROTOCOL_packet_size){
            PROTOCOL_packet_crc8 = _crc_ibutton_update(PROTOCOL_packet_crc8, read_byte);
            buf[PROTOCOL_current_index++] = read_byte;
        }
        else
        {
            // end of packet
            PROTOCOL_state = 0;
            if (PROTOCOL_packet_crc8 == read_byte){
                // crc ok
                return PROTOCOL_packet_size;
            }
            else {
                return -1;

            }
        }
    }
    return 0;
}
