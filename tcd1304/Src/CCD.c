/*
 * CCD.c
 *
 *  Created on: 11.03.2018
 *      Author: Mateusz
 */

#include "CCD.h"

/* Prepare CCD_Struct for Tx */
inline void CCD_Struct_Prepare(CCD_StructTypeDef* CCD_Struct)
{
	memcpy(&(CCD_Struct->sync_s), sync_str, sync_len);
	CCD_Struct->packet_size = CCD_Struct_packet_size;
	CCD_Struct->id = CCD_Struct_id;
	uint8_t crc = Crc8CQuick(crc0, CCD_Struct_packet_size, (uint8_t*) &(CCD_Struct->id));
	CCD_Struct->crc8 = crc;
}

/* Prepare CCD_Data for Tx */
void CCD_Data_Prepare(CCD_DataTypeDef* CCD_Data, Section s)
{
    if (s==low){
    	CCD_Struct_Prepare(&(CCD_Data->data_struct.low));
    }
    else if (s==high){
    	CCD_Struct_Prepare(&(CCD_Data->data_struct.high));
    }
}

/* Define and initialize global variables */

CCD_SettingsTypeDef CCD_settings = { .id = 'S',
		.SH_period = SH_period0,
		.ICG_period = ICG_period0,
		.SH_prescaler = SH_prescaler0,
		.ICG_prescaler = ICG_prescaler0,
		.N_avg = 1};

CCD_DataTypeDef CCD_data;
